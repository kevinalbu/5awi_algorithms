package at.albu.searchalgorithms;

import at.albu.exception.NumberNotFoundException;

public class SequenceSearch implements SearchAlgorithms {
	


	@Override
	public void constructor() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int search(int[] data, int target) throws NumberNotFoundException{
		int pos = 0;
		for (int i = 0; i < data.length; i++) {
			if (data[i] == target) {
				pos = i+1;
				return pos;
			}
		}
		if( pos == 0) {
			throw new NumberNotFoundException("Die Zahl ist nicht im Array vorhanden!");
		}
		return 0;
	}

}
