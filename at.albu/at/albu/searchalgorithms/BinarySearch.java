package at.albu.searchalgorithms;

import at.albu.exception.NumberNotFoundException;

public class BinarySearch implements SearchAlgorithms {

	@Override
	public void constructor() {
		// TODO Auto-generated method stub
	}

	@Override
	public int search(int[] data, int target) throws NumberNotFoundException {

		int size = data.length;
		int low = 0;
		int high = size - 1;
		int mid = (low + high) / 2;
		int pos = 0;

		while (low <= high) {
			mid = (low + high) / 2;
			if (data[mid] < target) {
				low = mid + 1;
			} else if (data[mid] == target) {
				pos = mid + 1;
				break;
			} else if (data[mid] > target) {
				high = mid - 1;
			}

		}
		if (low > high) {
			throw new NumberNotFoundException("Zahl nicht gefunden!");
		}

		return pos;
	}

}
