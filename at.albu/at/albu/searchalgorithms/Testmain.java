package at.albu.searchalgorithms;

import at.albu.exception.NumberNotFoundException;

public class Testmain {

	public static void main(String[] args) throws NumberNotFoundException {

		int[] array1 = { 17, 3, 6, 7, 2, 5 };

		// SequenceSearch ss = new SequenceSearch();
		//
		// try {
		// int y = ss.search(array1, 1);
		// System.out.println("Die Zahl befindet sich an " + y + ". Stelle");
		// } catch (Exception e) {
		// e.printStackTrace();
		// }

		BinarySearch ss = new BinarySearch();
		try {
			int y = ss.search(array1, 2);
			System.out.println("Die Zahl befindet sich an " + y + ". Stelle");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
