package at.albu.searchalgorithms;

import at.albu.exception.NumberNotFoundException;

public interface SearchAlgorithms {

	void constructor();
	int search(int[] data, int target) throws NumberNotFoundException;
	
}
