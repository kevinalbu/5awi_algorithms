package at.albu.main;

import at.albu.exception.NumberNotFoundException;
import at.albu.helper.*;
import at.albu.sorters.*;
import at.albu.searchalgorithms.*;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class Program {

	public static void main(String[] args) throws NumberNotFoundException {

		boolean found = false;
		String newLine = System.getProperty("line.separator");

		// Alte tests
		// int[] array1 = DataGenerator.randomDataGenerator(100,1,100);
		// System.out.println("Unsorted List: ");
		// DataGenerator.printData(array1);
		// int[] array2 = Selectionsorter.sort(array1);
		// System.out.println("Sorted List: ");
		// DataGenerator.printData(array1);

		@SuppressWarnings("resource")
		Scanner input = new Scanner(System.in);
		System.out.println("Enter number of elements:");
		int size = input.nextInt();
		System.out.println("Enter max value of elements:");
		int maxValue = input.nextInt();
		System.out.println("Enter min value of elements:");
		int minValue = input.nextInt();

		SortAlgorithm bs = new Insertionsorter();
		int[] RandomArray = DataGenerator.randomDataGenerator(size, minValue, maxValue);
		System.out.println("Unsorted: ");
		SortEngine se = new SortEngine();
		DataGenerator.printData(RandomArray);
		se.setAlgorithm(bs);
		System.out.println("Sorted: ");
		DataGenerator.printData(se.sort(RandomArray));

		BinarySearch ss = new BinarySearch();

		while (found == false) {
			try {
				TimeUnit.SECONDS.sleep(1);
				System.out.println("Enter number you would like to search for:");
				int x = input.nextInt();
				int y = ss.search(RandomArray, x);
				System.out.println("Die Zahl befindet sich an " + y + ". Stelle." + newLine + "END");
				found = true;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

}