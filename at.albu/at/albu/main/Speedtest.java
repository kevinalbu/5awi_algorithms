package at.albu.main;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import at.albu.helper.*;
import at.albu.sorters.*;

public class Speedtest {

	private List<SortAlgorithm> algorithms;

	public Speedtest() {
		this.algorithms = new ArrayList<>();
		addAlgorithm(new Bubblesorter());
		addAlgorithm(new Selectionsorter());
		addAlgorithm(new Insertionsorter());
	}

	public void addAlgorithm(SortAlgorithm algo) {
		this.algorithms.add(algo);
	}

	public void run() {

		int[] Data = DataGenerator.randomDataGenerator(10000, 1, 1000000);

		for (SortAlgorithm s : algorithms) {
			System.out.println(s.getName());
			final long timeStart = System.currentTimeMillis();
			s.sort(Data);
			final long timeEnd = System.currentTimeMillis();
			System.out.println("Delta Time = "
					// + (TimeUnit.MILLISECONDS.toSeconds(timeEnd) -
					// TimeUnit.MILLISECONDS.toSeconds(timeStart))
					+ (timeEnd - timeStart)
					// + " Sek.");
					+ " Millisek. ");
		}

	}

	public static void main(String[] args) {
		Speedtest st = new Speedtest();
		st.run();
	}
}
