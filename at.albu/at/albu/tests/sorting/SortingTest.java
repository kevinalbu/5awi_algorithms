package at.albu.tests.sorting;



public interface SortingTest {
	
	public void setUp();
	public void test();
	public boolean isSorted(int[] sorted);

}
