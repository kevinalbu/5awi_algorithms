package at.albu.tests.sorting;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import at.albu.sorters.*;

public class InsertionsortTest {

	private int[] data = new int[3];

	@Before
	public void setUp() throws Exception {
		data[0] = 17;
		data[1] = 3;
		data[2] = 173;
	}

	@Test
	public void test() {
		SortingTestHelper.test(new Insertionsorter(), data);
	}

	@Test
	public void testGetName() {
		fail("Not yet implemented");
	}

}
