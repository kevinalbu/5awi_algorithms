package at.albu.tests.sorting;

import static org.junit.Assert.assertTrue;

import at.albu.helper.SortAlgorithm;

public class SortingTestHelper {
	
	public static boolean test(SortAlgorithm sa, int[] data) {
		
		int[]unsorted = data.clone();
		int[] res = sa.sort(unsorted);
		
		return isSorted(res);
	}
	
	private static boolean isSorted(int[] sorted){
		int old=sorted[0];
		for(int i =1;i< sorted.length;i++){
			if(old<sorted[i]){
				old=sorted[i];
			}else{
				return false;
			}
		}
		return true;
	}

}
