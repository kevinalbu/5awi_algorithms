package at.albu.helper;

public interface SortAlgorithm {

	public int[] sort(int[] data);
	public String getName();

}
