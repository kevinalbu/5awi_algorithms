package at.albu.helper;

public class SortEngine {

	private SortAlgorithm sortAlgorithm;

	public int[] sort(int[] data) {
		return sortAlgorithm.sort(data);
	}

	public void setAlgorithm(SortAlgorithm algo) {
		this.sortAlgorithm = algo;
	}

}
