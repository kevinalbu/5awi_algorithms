package at.albu.helper;

import java.util.*;

public class DataGenerator {

	public static int[] randomDataGenerator(int size, int minValue, int maxValue) {
		
		int[] RandomArray = new int[size];
		
		for(int i = 0; i < RandomArray.length; i++){
			RandomArray[i] = (int) (Math.random() * (maxValue - minValue) + minValue);
		}
		
		return RandomArray;
		
	}
	
//	PrintData (Array)
	
//	public static void printData(int[] RandomArray){
//		
//		LinkedList LI  = new LinkedList(Arrays.asList(RandomArray));
//		
//		for (int i=0; i < RandomArray.length; i++){
//			System.out.println(LI[i]);
//		}
//		
//		
//	}
	
	
//  PrintData (LinkedList)	
	
	public static void printData(int[] data) {

		LinkedList<Integer> dataList = new LinkedList<Integer>();

		for (int n : data) {

			dataList.add(n);
		}

		System.out.println(dataList);
	}

}