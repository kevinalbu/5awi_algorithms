package at.albu.datastructures.recursion;

public class RecursionTest {
	
	public static void main(String[] args) {
		
	}
	
	private static int add(int number){
		int sum =0;
		
		while(sum<=100){
			sum += number;
		}
		return sum;
	}
	
	private static int addRec(int number, int cnt){
		if(cnt>3){
			return number;
		}else{
			return number + addRec(number, ++cnt);
		}
	}
	
	private static int getGGT(int a, int b) {
		System.out.println(a + ":" + b);
		if (a == b) {
			return a;
		} else if (a > b) {
			return getGGT(a - b, b);
		} else {
			return getGGT(a, b - a);
		}

	}
	
	private static int getFactorial(int x){
		if(x==1){
			return 1;
		} else {
			return x * getFactorial(--x);
		}

	}
	
	private static int getPotenz(int basis, int hochzahl){
		if(hochzahl==1){
			return basis;
		} else {
			return basis * getPotenz(basis, --hochzahl);
		}
	}
	
	private static void printBinary(int number){
		if(number==1 || number ==0){
			System.out.println(number);
		} else{
			printBinary(number/2);
			System.out.println(number%2);
		}
		
	}


	private static int getFibonacci(int location){
		if(location == 0){
	        return 0;
		}
	    else if(location == 1){
	      return 1;
	    }
	   else{
	      return getFibonacci(location - 1) + getFibonacci(location - 2);
	   }
	}
	
	private static boolean isPalindrom(String string){
		if(string.length()==0 || string.length()==1){
			return true;
		}else if(string.charAt(0) == string.charAt(string.length()-1)){
			return isPalindrom(string.substring(1,string.length()-1));
		} else{
			return false;
		}
	}

}
