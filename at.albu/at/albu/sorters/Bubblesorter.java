package at.albu.sorters;

import at.albu.helper.SortAlgorithm;

public class Bubblesorter implements SortAlgorithm {

	public int[] sort(int[] unsortedArray) {

		int size = unsortedArray.length;
		int temp = 0;

		for (int i = 0; i < size; i++) {
			for (int j = 1; j < (size - i); j++) {

				if (unsortedArray[j - 1] > unsortedArray[j]) {
					temp = unsortedArray[j - 1];
					unsortedArray[j - 1] = unsortedArray[j];
					unsortedArray[j] = temp;
				}

			}
		}
		return unsortedArray;

	}

	@Override
	public String getName() {
		return "Bubblesorter";
	}

}
