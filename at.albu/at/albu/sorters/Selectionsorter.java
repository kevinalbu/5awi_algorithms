package at.albu.sorters;

import at.albu.helper.SortAlgorithm;

public class Selectionsorter implements SortAlgorithm {

	public int[] sort(int[] unsortedArray) {

		int min = 0;
		int size = unsortedArray.length;
		for (int i = 0; i < size; i++) {
			min = i;
			for (int j = i + 1; j < size; j++) {
				if (unsortedArray[j] < unsortedArray[min]) {
					min = j;

				}
			}
			if (min != i) {
				int temp = unsortedArray[i];
				unsortedArray[i] = unsortedArray[min];
				unsortedArray[min] = temp;
			}
		}
		return unsortedArray;

	}

	@Override
	public String getName() {
		return "Selectionsorter";
	}
}
