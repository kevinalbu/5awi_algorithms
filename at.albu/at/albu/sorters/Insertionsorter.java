package at.albu.sorters;

import at.albu.helper.SortAlgorithm;

public class Insertionsorter implements SortAlgorithm {

	public int[] sort(int[] unsortedArray) {

		 for (int i = 1; i < unsortedArray.length; i++) {
             for(int j = i ; j > 0 ; j--){
                 if(unsortedArray[j] < unsortedArray[j-1]){
                     int a = unsortedArray[j];
                     unsortedArray[j] = unsortedArray[j-1];
                     unsortedArray[j-1] = a;
                 }
             }
         }
         return unsortedArray;


	}

	@Override
	public String getName() {
		return "Insertionsorter";
	}

}
